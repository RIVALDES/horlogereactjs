import React from 'react'


const Time = (props) => {


    return (
        <h1>
            {props.hours >= 10 ? props.hours : '0' + props.hours}  :
            {props.minute >= 10 ? props.minute : '0' + props.minute} :
            {props.seconds >= 10 ? props.seconds : '0' + props.seconds}
        </h1>
    )
}

export default Time;