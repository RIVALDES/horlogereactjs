import '../../App.css';
import FormModal from '../FormModal';
import Board from '../Board';
import React from 'react';

let time = new Date();
class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            year: time.getFullYear(),
            months: time.getMonth(),
            day: time.getDay(),
            hours: time.getHours(),
            minute: time.getMinutes(),
            seconds: time.getSeconds(),
            date: time.getDate(),
            modal: false,
            inputhours: 0,
            inputmin: 0,
            inputsec: 0,
            inputdate:null,
        }

    }

    componentDidMount() {
        setInterval(() => {
            time.setSeconds(time.getSeconds() + 1)
            this.setState({
                year: time.getFullYear(),
                months: time.getMonth(),
                day: time.getDay(),
                hours: time.getHours(),
                minute: time.getMinutes(),
                seconds: time.getSeconds(),
                date: time.getDate(),

            })
        }, 1000)
    }


    render() {
        return (
            <div className="App">
                <FormModal
                    modal={this.state.modal} hideModal={this.hideModal} handeleSubmit={this.handeleSubmit}
                    handeleinputhour={this.handeleinputhour} handeleinputmin={this.handeleinputmin} handeleinputsec={this.handeleinputsec}
                    hours={this.state.hours} seconds={this.state.seconds} minute={this.state.minute}
                    inputhours={this.state.inputhours} inputmin={this.state.inputmin} inputsec={this.state.inputsec}
                    showModal={this.showModal} handleInputDate={this.handleInputDate}
                />

                <Board
                    day={this.state.day} hours={this.state.hours} minute={this.state.minute} date={this.state.date}
                    seconds={this.state.seconds} months={this.state.months} year={this.state.year}
                    showModal={this.showModal}
                />
            </div>
        )
    }


    handeleSubmit = (e) => {
        e.preventDefault();
        time.setMinutes(this.state.inputmin);
        time.setHours(this.state.inputhours)
        time.setSeconds(this.state.inputsec)
        time.setMonth(this.state.inputdate.getMonth());
        time.setFullYear(this.state.inputdate.getFullYear());
        time.setDate( this.state.inputdate.getDate() );


        this.setState({ modal: false });
    }

    showModal = () => {
        this.setState({ inputmin: this.state.minute, inputhours: this.state.hours, inputsec: this.state.inputsec, inputdate:new Date()});
        this.setState({ modal: true })
    }

    handeleinputhour = (e) => {
        this.setState({ inputhours: e.target.value })
    }
    
    handeleinputmin = (e) => {
        this.setState({ inputmin: e.target.value })
    }

    handeleinputsec = (e) => {
        this.setState({ inputsec: e.target.value })
    }

    hideModal = () => {
        this.setState({ modal: false });

    }

    handleInputDate=(e)=>{
        this.setState({inputdate:new Date(e.target.valueAsNumber)}) 
    }

}

export default App;
