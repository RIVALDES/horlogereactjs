import React from 'react'

const Date = (props) => {

    const months = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aough', 'Septembre', 'Octobre', 'Novembre', 'Decembre']

    return (
        <h1>
            {props.date > 10 ? props.date : '0' + props.date} &nbsp;
            {months[props.months]} &nbsp;
            {props.year}&nbsp;

        </h1>
    )
}

export default Date;