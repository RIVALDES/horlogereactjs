import React from 'react'
import Day from '../Day'
import Time from '../Time';
import Dates from '../Date';
import DayQualifer from '../DayQualifer';

const Board = (props) => {

    return (

        <div className="bigContainer">
            <div className="container">
                <Day day={props.day} />
                <DayQualifer hours={props.hours} />
                <Time hours={props.hours} minute={props.minute} seconds={props.seconds} />
                <Dates date={props.date} months={props.months} year={props.year} />
                <button onClick={props.showModal}>
                    modifier l'heure
                </button>
            </div>
        </div>
    )


}

export default Board;