import React from 'react'


const Day = (props) =>{

    const week = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi']

    return(
        <h1> {week[props.day]}</h1>
    )
}

export default Day;