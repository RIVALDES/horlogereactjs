import React from 'react'


class FormModal extends React.Component {

    render() {
        const time = new Date();
        const  format = time.toISOString().substring(0,10)
        return (
            <div className="modal" style={{ display: this.props.modal ? 'flex' : 'none' }}>
               
                <div>
                    <span onClick={this.props.hideModal}>X</span>
                </div>

                <form onSubmit={this.props.handeleSubmit} >
                    <h2> Modification de Date</h2>
                    <div>
                        <label htmlFor="heure">heure</label>
                        <input type='number' onChange={this.props.handeleinputhour} defaultValue={this.props.hours}/>
                    </div>

                    <div>
                        <label htmlFor="minute">Minute</label>
                        <input type='number' onChange={this.props.handeleinputmin} defaultValue={this.props.minute} />
                    </div>

                    <div>
                        <label> Seconde</label>
                        <input type='number' onChange={this.props.handeleinputsec} defaultValue={this.props.seconds} />
                    </div>

                    <div>
                        <label> date</label>
                        <input type='date' onChange={this.props.handleInputDate}  defaultValue={format}/>
                    </div>
                    
                    <div>
                        <button type='reset' onClick={this.props.hideModal}>Annuler</button>
                        <button type='submit'
                            disabled={!(this.props.inputhours < 24 && this.props.inputmin < 60 && this.props.inputsec < 60)}
                        >Enregistrer
                        </button>
                    </div>
                </form>
            </div>
        )
    }




}
export default FormModal;